//
// Created by patri on 11/25/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define length 100000

int* criar_vetor() {
    int* matriz = (int*) malloc(length * sizeof(int*));
    for (int i = 0; i < length; i++) matriz[i] = rand() % 100 + 1;

    return matriz;
}

void remover_pares_ou_multiplos_de_cinco(int* vetor, int doisOuCinco) {
    long size = length;

    for (int i = size - 1; i >= 0; i--) {
        if (vetor[i] % doisOuCinco == 0 && vetor[i] != -1) {

            for (int j = i; j < size - 1; j++) {
                vetor[j] = vetor[j + 1];
            }

            size--;
            vetor[i] = -1;
        }
    }
}

void* remover_pares(void* args) {
    int* vetor = (int*) args;
    remover_pares_ou_multiplos_de_cinco(vetor, 2);
    return NULL;
}

void* remover_multiplos_de_cinco(void* args) {
    int* vetor = (int*) args;
    remover_pares_ou_multiplos_de_cinco(vetor, 5);
    return NULL;
}

void imprimir_vetor(int* vetor) {
    for (int i = 0; i < length; i++) {
        if (vetor[i] != -1) {
            printf("%d ", vetor[i]);
        }
    }
    printf("\n");
}

int* verifica_resultado(int* vetor){
    int* correcao = criar_vetor();

    remover_pares_ou_multiplos_de_cinco(correcao, 2);
    remover_pares_ou_multiplos_de_cinco(correcao, 5);

    int iguais = 1;
    for (int i = 0; i < length; i++) {
        if (vetor[i] != correcao[i]) {
            iguais = 0;
            break;
        }
    }

    if (iguais) printf("Vetores iguais!\n");
    else printf("Vetores diferentes!\n");

    return correcao;
}

int main() {
    struct timespec inicio, fim;
    int* vetor = criar_vetor();

    pthread_t thread_pares;
    pthread_t thread_multiplos_de_cinco;

    clock_gettime(CLOCK_MONOTONIC, &inicio);
    pthread_create(&thread_pares, NULL, remover_pares, vetor);
    pthread_create(&thread_multiplos_de_cinco, NULL, remover_multiplos_de_cinco, vetor);

    pthread_join(thread_pares, NULL);
    pthread_join(thread_multiplos_de_cinco, NULL);

    clock_gettime(CLOCK_MONOTONIC, &fim);

    double tempo_gasto = (fim.tv_sec - inicio.tv_sec) * 1000 + (fim.tv_nsec - inicio.tv_nsec) / 1000000;
    printf("Tempo gasto em milissegundos com thread: %f\n", tempo_gasto);

    clock_gettime(CLOCK_MONOTONIC, &inicio);
    int* correcao = verifica_resultado(vetor);
    clock_gettime(CLOCK_MONOTONIC, &fim);

    tempo_gasto = (fim.tv_sec - inicio.tv_sec) * 1000 + (fim.tv_nsec - inicio.tv_nsec) / 1000000;
    printf("Tempo gasto em milissegundos sem thread: %f\n", tempo_gasto);

    free(vetor);
    free(correcao);

    return 0;
}