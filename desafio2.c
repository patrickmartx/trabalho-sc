#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#define length 10000

int** criar_matriz_quadrada() {
    int** matriz = (int**)malloc(length * sizeof(int*));

    for (int i = 0; i < length; i++) {
        matriz[i] = (int*)malloc(length * sizeof(int));
        for (int j = 0; j < length; j++) {
            matriz[i][j] = 10;
        }
    }

    return matriz;
}

int** somar_matriz_quadrada(int** matriz1, int** matriz2) {
    int** matrizResultado = (int**)malloc(length * sizeof(int*));

    for (int i = 0; i < length; i++) {
        matrizResultado[i] = (int*)malloc(length * sizeof(int));
        for (int j = 0; j < length; j++) {
            matrizResultado[i][j] = matriz1[i][j] + matriz2[i][j];
        }
    }

    return matrizResultado;
}
struct Arg {
        int inicio;
        int fim;
        int** matriz1;
        int** matriz2;
        int** matrizResultado;
};

void *somar_parte_da_matriz(void *arg) {
    struct Arg *args = (struct Arg *)arg;
    for (int i =  args->inicio; i < args->fim; i++) {
        args->matrizResultado[i] = (int*)malloc(length * sizeof(int));
        for (int j = 0; j < length; j++) {
            args->matrizResultado[i][j] = args->matriz1[i][j] + args->matriz2[i][j];
        }
    }
    return NULL;
}

void print_matriz(int** matriz) {
    for (int i = 0; i < length; i++) {
        for (int j = 0; j < length; j++) {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
}

void liberar_matriz(int** matriz) {
    for (int i = 0; i < length; i++) {
        free(matriz[i]);
    }
    free(matriz);
}

int main() {
    struct timespec inicio, fim;
    int** matriz1 = criar_matriz_quadrada();
    int** matriz2 = criar_matriz_quadrada();
    printf("Tempo em milissegundos da soma de matriz %d x %d:\n", length, length);
    
    // Sem thread:
    clock_gettime(CLOCK_MONOTONIC, &inicio);
    int** matrizResultado = somar_matriz_quadrada(matriz1, matriz2);
    clock_gettime(CLOCK_MONOTONIC, &fim);
    liberar_matriz(matrizResultado);

    double tempo_gasto = (fim.tv_sec - inicio.tv_sec) * 1000 + (fim.tv_nsec - inicio.tv_nsec) / 1000000;
    printf("Sem thread %f\n", tempo_gasto);
    
    // Com thread
    matrizResultado = (int**) malloc(length * sizeof(int*));
    struct Arg primeira_parte = {
        .inicio = 0,
        .fim = length / 4,
        .matriz1 = matriz1,
        .matriz2 = matriz2,
        .matrizResultado = matrizResultado
    };
    struct Arg segunda_parte = {
        .inicio = length / 4,
        .fim = (length / 4) * 2,
        .matriz1 = matriz1,
        .matriz2 = matriz2,
        .matrizResultado = matrizResultado
    };
    struct Arg terceira_parte = {
        .inicio = (length / 4) * 2,
        .fim = (length / 4) * 3,
        .matriz1 = matriz1,
        .matriz2 = matriz2,
        .matrizResultado = matrizResultado
    };
    struct Arg quarta_parte = {
        .inicio = (length / 4) * 3,
        .fim = length,
        .matriz1 = matriz1,
        .matriz2 = matriz2,
        .matrizResultado = matrizResultado
    };

    struct Arg partes[4] = {primeira_parte, segunda_parte, terceira_parte, quarta_parte};
    pthread_t threads[4];
    
    clock_gettime(CLOCK_MONOTONIC, &inicio);

    for(int i = 0; i < 4; i++) {
        struct Arg *parte = malloc(sizeof(struct Arg));
        *parte = partes[i];
        pthread_create(&threads[i], NULL, somar_parte_da_matriz, parte);
    }

    for(int i = 0; i < 4; i++) pthread_join(threads[i], NULL);
    
    clock_gettime(CLOCK_MONOTONIC, &fim);
    liberar_matriz(matrizResultado);

    tempo_gasto = (fim.tv_sec - inicio.tv_sec) * 1000.0 + (fim.tv_nsec - inicio.tv_nsec) / 1000000.0;
    printf("Com thread: %f\n", tempo_gasto);
    liberar_matriz(matriz1);
    liberar_matriz(matriz2);

    return 0;
}
