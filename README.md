# Trabalho de Sistemas Computacionais

## Desenvolvedores:
* [Patrick Martins](https://gitlab.com/patrickmartx)
* [Patrick Melo](https://gitlab.com/pamedsi)
---
## Desafio

### Parte 1

Faça um programa que crie "N" threads ("N" é um dado de entrada lido do teclado) usando a biblioteca pthreads. Cada thread, quando executar, deverá imprimir na tela "Eu sou a thread com TID YY e ZZ", onde YY é o TID obtido usando a função pthread_self() e ZZ é o TID obtido com a função gettid. Imprima TID usando %lu. 

Explique as saídas do programa quando este é executado. 
gettid e pthread_self retornam o mesmo valor? Se não, pesquise qual a diferença entre elas.
Para chamar gettid, use instrução syscall(SYS_gettid) e faça include da biblioteca 	``` <sys/syscall.h> ```.

### Parte 2

+ Faça um programa que some duas matrizes quadradas A e B de dimensões 500x500 e que armazenem números inteiros. 

+ Faça a soma sem usar threads e com 4 threads (uma thread para cada parte da matriz).

+ Compare o tempo de execução das duas versões. Meça o tempo com precisão de nanossegundos. 

### Parte 3

Implementar o seguinte programa:

+ Criar um vetor de inteiros de tamanho 100.000.

+ Preencher o vetor com números aleatórios entre 1 e 100.
   * Mude o seed quando achar que terminou e rode algumas vezes para ver se não tem erros.

+ Criar 2 threads:
	* T1 remove números pares do vetor.
	* T2 remove números múltiplos de 5 do vetor.
	
+ Remova do fim para o começo do vetor e mantenha a ordem original dos elementos.

+ Teste o programa com e sem semáforos (ambos com threads), observe o resultado e explique o que está acontecendo.
	
+ Compare o tempo de execução sem threads e com threads + semáforos. Use alguma biblioteca para pegar o tempo atual do sistema. Qual o mais rápido?

+ (*OBRIGATÓRIO*) Construa uma função que avalia se o resultado final está correto, ou seja:
	* faça remoção de múltiplos de 2 e 5 sequencialmente (ou seja, sem usar threads).
	* compare resultado com saída da versão com threads.
