#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <pthread.h>
#include <unistd.h>

void *printarMensagem(void * param){
    pthread_t tid_pthread = pthread_self();
    pid_t tid_gettid = syscall(SYS_gettid);

    printf("Eu sou a thread com TID %lu e %lu\n", tid_pthread, tid_gettid);
}

int main() {
    int numeroThreads;
    int i;

    printf("Digite o número de threads (N): ");
    scanf("%d", &numeroThreads);

    pthread_t threads[numeroThreads];
 
    for(i=0;i<numeroThreads;i++)
       pthread_create(&threads[i], NULL, printarMensagem, NULL);

    for(i=0;i<numeroThreads;i++)
      pthread_join(threads[i],NULL);

    return 0;
}
